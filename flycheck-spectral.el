;;; flycheck-spectral.el --- A Flycheck parser using Spectral  -*- lexical-binding: t; -*-

;; Copyright 2023 Alex Figl-Brick

;; Author: Alex Figl-Brick <alex@alexbrick.me>
;; Package-Requires: ((emacs "27.1"))
;; Version: 0.1

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see
;; <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is a Flychecker for API descriptions using Spectral:
;; https://stoplight.io/open-source/spectral

;;; Code:

(require 'flycheck)

(defun flycheck-spectral--parse-error (buffer checker json-error)
  "Parse the JSON-ERROR for BUFFER for CHECKER into a Flycheck error."
  (let-alist json-error
    (flycheck-error-new
     :line (1+ .range.start.line)
     :column (1+ .range.start.character)
     :end-line (1+ .range.end.line)
     :end-column (1+ .range.end.character)
     :message .message
     :id .code
     :buffer buffer
     :checker checker
     :level (if (= 0 .severity) 'error 'warning)
     :filename .source)))

(defun flycheck-spectral--parser (output checker buffer)
  "Parse OUTPUT as JSON to find errors for BUFFER from CHECKER."
  (let ((json (json-parse-string output :array-type 'list :object-type 'alist)))
    (mapcar (apply-partially #'flycheck-spectral--parse-error buffer checker) json)))

(defun flycheck-spectral--predicate ()
  "Determine if the current buffer should run the spectral flychecker.

Currently we only run for OpenAPI files."
  (equal "openapi" (file-name-base (or buffer-file-name (buffer-name)))))

(flycheck-def-config-file-var
    flycheck-spectral-config-file
    spectral
    '(".spectral.yml" ".spectral.yaml" ".spectral.json" ".spectral.js"))

(flycheck-define-checker spectral
  "A syntax checker for API definitions using Spectral."
  :command ("spectral" "lint" "-f" "json" "--quiet" (config-file "--ruleset" flycheck-spectral-config-file) source)
  :error-parser flycheck-spectral--parser
  :modes yaml-mode
  :predicate flycheck-spectral--predicate)

(add-to-list 'flycheck-checkers 'spectral)

(provide 'flycheck-spectral)

;;; flycheck-spectral.el ends here
